package com.schibsted.friends.unit.repository;

import com.schibsted.friends.model.user.User;
import com.schibsted.friends.repository.UserRepository;
import com.schibsted.friends.unit.controller.MockUserData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class UserRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    private User user;

    @Before
    public void setUp() {
	user = MockUserData.createOneUserNoId("johnsnow");
    }

    @Test
    public void findAll() throws Exception {
	//given
	entityManager.persist(user);
	entityManager.flush();

	//when
	List<User> usersList = userRepository.findAll();

	//then
	assertThat(usersList.size()).isEqualTo(6);
	assertThat(usersList.get(5)).isEqualTo(user);
    }

    @Test
    public void findAllByUsername() throws Exception {
	//given
	entityManager.persist(user);
	entityManager.flush();

	//when
	User allByUsername = userRepository.findByUsername(user.getUsername()).get();

	//then
	assertThat(allByUsername.getUsername()).isEqualTo(user.getUsername());
    }

    @Test
    public void deleteByUsername() throws Exception {
	//given
	entityManager.persist(user);
	entityManager.flush();

	//when
	userRepository.delete(user);
	List<User> usersList = userRepository.findAll();

	//then
	assertThat(usersList.size()).isEqualTo(5);
    }
}
