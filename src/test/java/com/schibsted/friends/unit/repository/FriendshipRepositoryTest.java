package com.schibsted.friends.unit.repository;

import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.repository.FriendshipRepository;
import com.schibsted.friends.unit.controller.MockUserData;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class FriendshipRepositoryTest {

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private FriendshipRepository friendshipRepository;

    private Friendship friendship;

    @Before
    public void setUp() {
	friendship = MockUserData.createOneFriendshipNoId();
    }

    @Test
    public void findAll() throws Exception {
	//given
	entityManager.persist(friendship);
	entityManager.flush();

	//when
	List<Friendship> usersList = friendshipRepository.findAll();

	//then
	assertThat(usersList.size()).isEqualTo(4);
	assertThat(usersList.get(3)).isEqualTo(friendship);
    }

    @Test
    public void findAllByUsername() throws Exception {
	//given
	entityManager.persist(friendship);
	entityManager.flush();

	//when
	List<Friendship> allByUsername = friendshipRepository.findByUsername(friendship.getUsername());

	//then
	assertThat(allByUsername.get(0).getUsername()).isEqualTo(friendship.getUsername());
    }

    @Test
    public void deleteByUsername() throws Exception {
	//given
	entityManager.persist(friendship);
	entityManager.flush();

	//when
	friendshipRepository.delete(friendship);
	List<Friendship> friendshipsList = friendshipRepository.findAll();

	//then
	assertThat(friendshipsList.size()).isEqualTo(3);
    }
}
