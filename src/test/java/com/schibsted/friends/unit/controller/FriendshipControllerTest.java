package com.schibsted.friends.unit.controller;

import com.schibsted.friends.controller.FriendshipController;
import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import com.schibsted.friends.model.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.schibsted.friends.constant.Paths.*;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(FriendshipController.class)
public class FriendshipControllerTest extends BaseControllerTest {

    private static final Logger logger = LogManager.getLogger(FriendshipControllerTest.class);

    @Test
    public void whenGetAllFriendships_thenReturnFriendships() throws Exception {
		List<Friendship> friendships = MockUserData.createSomeFriendships();

		given(friendshipController.getAllFriendships()).willReturn(ResponseEntity.ok(friendships));

		mockMvc.perform(get(VERSION + FRIENDSHIPS)
			.with(user("arya1").password("12345678"))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(3)))
			.andExpect(jsonPath("$[0].username", is(friendships.get(0).getUsername())));
    }

	@Test
	public void givenNoAuthentication_whenGetFriendships_thenReturnUnauthorization() throws Exception {
		List<Friendship> friendships = MockUserData.createSomeFriendships();

		given(friendshipController.getAllFriendships()).willReturn(ResponseEntity.ok(friendships));

		mockMvc.perform(get(VERSION + FRIENDSHIPS)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
	}

    @Test
    public void givenOneUser_whenGetPendingFriendships_thenReturnPendings() throws Exception {
		User user = MockUserData.createOneUser("margaery");
        List<Friendship> friendships = MockUserData.createSomeFriendships();

		given(friendshipController.getPendingsFriendships(user.getUsername())).willReturn(ResponseEntity.ok(friendships));

		mockMvc.perform(get(VERSION + FRIENDSHIPS + PENDINGS + user.getUsername())
                .param(USERNAME_PARAM, user.getUsername())
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].username", is(friendships.get(0).getUsername())));
    }

    @Test
    @Ignore
    public void givenOneUser_requestFriendshipToAnotherValid_thenReturnPending() throws Exception {
        User user = MockUserData.createOneUser("margaery");
        user.setPassword("12345678");
        Friendship friendship = MockUserData.createOneFriendship();

        given(friendshipController.requestFriendship(user, "ygritte")).willReturn(ResponseEntity.ok(friendship));

        mockMvc.perform(put(VERSION + FRIENDSHIPS + REQUEST)
                .param(FRIEND_PARAM, "ygritte")
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(createUserInJson(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(FriendshipStatus.PENDING)));
    }

    @Test
    @Ignore
    public void givenOneUser_acceptFriendshipToAnotherValid_thenReturnAccepted() throws Exception {
        User user = MockUserData.createOneUser("ygritte");
        Friendship friendship = MockUserData.createOneFriendship();

        given(friendshipController.requestFriendship(user, "margaery")).willReturn(ResponseEntity.ok(friendship));

        mockMvc.perform(post(VERSION + FRIENDSHIPS + ACCEPT)
                .param(USERNAME_PARAM, "margaery")
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(createUserInJson(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is(FriendshipStatus.ACCEPTED)));
    }

    @Test
    @Ignore
    public void givenOneUser_declineFriendshipToAnotherValid_thenReturnDeclined() throws Exception {
        User user = MockUserData.createOneUser("ygritte");
        Friendship friendship = MockUserData.createOneFriendship();

        given(friendshipController.declineFriendship(user, "margaery")).willReturn(ResponseEntity.ok(friendship));

        mockMvc.perform(post(VERSION + FRIENDSHIPS + DECLINE)
                .param(USERNAME_PARAM, "cersei")
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(createUserInJson(user)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.username", is(FriendshipStatus.DECLINED)));
    }
}
