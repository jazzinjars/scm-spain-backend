package com.schibsted.friends.unit.controller;

import com.schibsted.friends.controller.UserController;
import com.schibsted.friends.model.user.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.schibsted.friends.constant.Paths.*;
import static java.util.Collections.singletonList;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest extends BaseControllerTest {

    private static final Logger logger = LogManager.getLogger(UserControllerTest.class);

    @Test
    public void givenOneUser_whenGetAllUsers_thenReturnThisUser() throws Exception {
        User user = MockUserData.createOneUser("johnsnow");
		List<User> users = singletonList(user);

		given(userController.getAllUsers()).willReturn(ResponseEntity.ok(users));

		mockMvc.perform(get(ALL_USERS)
			.with(user("arya1").password("12345678"))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$", hasSize(1)))
			.andExpect(jsonPath("$[0].username", is(user.getUsername())));
    }

    @Test
    public void givenOneUser_whenGetUserByUsername_thenReturnThisUser() throws Exception {
		User user = MockUserData.createOneUser("johnsnow");

		given(userController.getUser(user.getUsername())).willReturn(ResponseEntity.ok(user));

		mockMvc.perform(get(VERSION + USERS + user.getUsername())
			.with(user("arya1").password("12345678"))
			.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.username", is(user.getUsername())));
    }

    @Test
    public void givenNoAuthentication_whenGetUserByUsername_thenReturnUnauthorization() throws Exception {
        User user = MockUserData.createOneUser("johnsnow");

        given(userController.getUser(user.getUsername())).willReturn(ResponseEntity.ok(user));

        mockMvc.perform(get(VERSION + USERS + user.getUsername())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void givenInvalidCredentials_whenSignup_thenException() throws Exception {
        User user = MockUserData.createOneUser("johnsnow");

        given(userController.signupUser(user.getUsername(), user.getPassword())).willReturn(ResponseEntity.ok(user));

        HttpSessionCsrfTokenRepository httpSessionCsrfTokenRepository = new HttpSessionCsrfTokenRepository();
        CsrfToken csrfToken = httpSessionCsrfTokenRepository.generateToken(new MockHttpServletRequest());

        mockMvc.perform(post(VERSION + USERS + SIGNUP)
                .param(USERNAME_PARAM, "1")
                .param(PASSWORD_PARAM, "1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is4xxClientError());
    }

    @Test
    @Ignore
	public void givenValidCredentials_whenSignup_thenReturnUser() throws Exception {
        User user = MockUserData.createOneUser("johnsnow");

        given(userController.signupUser(user.getUsername(), user.getPassword())).willReturn(ResponseEntity.ok(user));

        mockMvc.perform(post(VERSION + USERS + SIGNUP)
                .param(USERNAME_PARAM, user.getUsername())
                .param(PASSWORD_PARAM, "12345678")
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is(user.getStatus()))).andDo(print());
	}

	@Test
    @Ignore
    public void givenValidUser_whenGetFriends_thenReturnUserFriends() throws Exception {
        User user = MockUserData.createOneUser("johnsnow");
        List<User> friends = singletonList(user);

        given(userController.getUserFriends(user.getUsername())).willReturn(ResponseEntity.ok(friends));

        mockMvc.perform(get(VERSION + USERS + FRIENDS)
                .param(USERNAME_PARAM, user.getUsername())
                .with(user("arya1").password("12345678"))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].status", is(user.getStatus()))).andDo(print());
    }
}
