package com.schibsted.friends.unit.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.schibsted.friends.controller.FriendshipController;
import com.schibsted.friends.controller.UserController;
import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

public class BaseControllerTest {

    static final String RESOURCE_BASE = "/friends/v1";
    static final String ALL_USERS = RESOURCE_BASE + "/users";
    static final String USERNAME_PARAM = "username";
    static final String PASSWORD_PARAM = "password";
    static final String FRIEND_PARAM = "friend";

    @Autowired
    protected MockMvc mockMvc;

    @MockBean
    protected UserController userController;

    @MockBean
    protected FriendshipController friendshipController;

    static String createUserInJson (User user) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(user);
    }

    static String createFriendshipInJson (Friendship friendship) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(friendship);
    }
}
