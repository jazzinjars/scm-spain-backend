package com.schibsted.friends.unit.controller;

import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.model.user.UserRole;
import com.schibsted.friends.model.user.UserStatus;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

public class MockUserData {

    public static List<User> createSomeUsers() {
        List<User> users = Arrays.asList(
                new User(1234L, "margaery", "$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.", LocalDateTime.now(), UserStatus.NOFRIENDS, UserRole.USER,false, 0),
                new User(1235L, "ygritte", "$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.", LocalDateTime.now(), UserStatus.NOFRIENDS, UserRole.USER,false, 0),
                new User(1236L, "melisandre", "$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.", LocalDateTime.now(), UserStatus.NOFRIENDS, UserRole.USER,false, 0));
        return users;
    }

    public static List<Friendship> createSomeFriendships() {
        List<Friendship> friendships = Arrays.asList(
                new Friendship(2234L, "margaery", "ygritte", FriendshipStatus.PENDING, LocalDateTime.now()),
                new Friendship(2235L, "melisandre", "ygritte", FriendshipStatus.PENDING, LocalDateTime.now()),
                new Friendship(2236L, "melisandre", "margaery", FriendshipStatus.PENDING, LocalDateTime.now()));
        return friendships;
    }

    public static Friendship createOneFriendship() {
        Friendship friendship = new Friendship(2234L, "margaery", "ygritte", FriendshipStatus.PENDING, LocalDateTime.now());
        return friendship;
    }

    public static Friendship createOneFriendshipNoId() {
        Friendship friendship = new Friendship();
        friendship.setStatus(FriendshipStatus.PENDING);
        friendship.setFriendsDate(LocalDateTime.now());
        friendship.setUsername("margaery");
        friendship.setFriendof("ygritte");
        return friendship;
    }

    public static User createOneUser(String username) {
        return new User(1234L, username, "$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.", LocalDateTime.now(), UserStatus.NOFRIENDS, UserRole.USER,false, 0);
    }

    public static User createOneUserNoId(String username) {
        User user = new User();
        user.setDeclinations(0);
        user.setHidden(false);
        user.setPassword("$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.");
        user.setRole(UserRole.ADMIN);
        user.setSignup(LocalDateTime.now());
        user.setStatus(UserStatus.NOFRIENDS);
        user.setUsername(username);
        return user;
    }
}
