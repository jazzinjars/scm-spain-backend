package com.schibsted.friends.rest;

import org.junit.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.apache.http.HttpStatus.SC_METHOD_NOT_ALLOWED;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.lessThan;

public class FriendshipTest extends BaseTest {

    @Test
    public void checkResponseTimeAll() {
	prepareGet(ALL_FRIENDSHIPS)
		.time(lessThan(2000L));
    }

    @Test
    public void checkArrivalEndpointStatus() {
	prepareGet(ALL_FRIENDSHIPS).statusCode(SC_OK);
    }

    @Test
    public void checkSchemaValidity() {
	prepareGet(ALL_FRIENDSHIPS)
		.assertThat()
		.body(matchesJsonSchemaInClasspath("schemas/friendship-schema.json"));
    }

    @Test
    public void checkResponseTimeById() {
	prepareGet(PENDING_FRIENDSHIPS + USERNAME)
		.time(lessThan(ENDPOINT_RESPONSE_TIME));
    }

    @Test
    public void checkPutMethod() {
	preparePut(PENDING_FRIENDSHIPS, DUMMY_TEST_JSON)
		.then()
		.statusCode(SC_METHOD_NOT_ALLOWED);
    }

    @Test
    public void checkPostMethod() {
	preparePost(ALL_FRIENDSHIPS, DUMMY_TEST_JSON)
		.then()
		.statusCode(SC_METHOD_NOT_ALLOWED);
    }
}
