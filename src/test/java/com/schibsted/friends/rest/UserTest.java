package com.schibsted.friends.rest;

import org.junit.Test;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.apache.http.HttpStatus.SC_METHOD_NOT_ALLOWED;
import static org.apache.http.HttpStatus.SC_OK;
import static org.hamcrest.Matchers.lessThan;

public class UserTest extends BaseTest {

    @Test
    public void checkResponseTimeAll() {
	prepareGet(ALL_USERS)
		.time(lessThan(2000L));
    }

    @Test
    public void checkArrivalEndpointStatus() {
	prepareGet(ALL_USERS).statusCode(SC_OK);
    }

    @Test
    public void checkSchemaValidity() {
	prepareGet(ALL_USERS)
		.assertThat()
		.body(matchesJsonSchemaInClasspath("schemas/user-schema.json"));
    }

    @Test
    public void checkResponseTimeById() {
	prepareGet(USER_BY_USERNAME)
		.time(lessThan(ENDPOINT_RESPONSE_TIME));
    }

    @Test
    public void checkPutMethod() {
	preparePut(USER_BY_USERNAME, DUMMY_TEST_JSON)
		.then()
		.statusCode(SC_METHOD_NOT_ALLOWED);
    }

    @Test
    public void checkPostMethod() {
	preparePost(ALL_USERS, DUMMY_TEST_JSON)
		.then()
		.statusCode(SC_METHOD_NOT_ALLOWED);
    }
}
