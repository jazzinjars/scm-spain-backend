package com.schibsted.friends.rest;

import com.schibsted.friends.FriendsApplication;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static io.restassured.RestAssured.given;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@RunWith(SpringRunner.class)
@SpringBootTest(
        classes = FriendsApplication.class,
        webEnvironment = RANDOM_PORT
)
@ActiveProfiles("test")
public abstract class BaseTest {

    static final String ALL_USERS = "users";
    static final String USER_BY_USERNAME = ALL_USERS + "/arya1";
    static final String GET_FRIENDS_BY_USERNAME = ALL_USERS + "/friends";
    static final String ALL_FRIENDSHIPS = "friendships";
    static final String PENDING_FRIENDSHIPS = ALL_FRIENDSHIPS + "/pendings";
    static final long ENDPOINT_RESPONSE_TIME = 600L;
    static final String DUMMY_TEST_JSON = "{ \"Test\": \"Test\" }";

    static final String USERNAME = "arya1";
    private static final String PASSWORD = "12345678";
    private static final String HOST_ROOT = "http://localhost/friends/v1/";

    @LocalServerPort
    private int port;

    ValidatableResponse prepareGet(String path) {
        return prepareGetDeleteWhen()
                .get(HOST_ROOT + path)
                .then();
    }

    ValidatableResponse prepareDelete(String path) {
        return prepareGetDeleteWhen()
                .delete(HOST_ROOT + path)
                .then();
    }

    Response preparePut(String path, String body) {
        return preparePostPutWhen(body)
                .put(HOST_ROOT + path);
    }

    Response preparePost(String path, String body) {
        return preparePostPutWhen(body)
                .post(HOST_ROOT + path);
    }

    private RequestSpecification preparePostPutWhen(String body) {
        return given()
                .port(port)
                .auth()
                .basic(USERNAME, PASSWORD)
                .contentType(String.valueOf(APPLICATION_JSON))
                .body(body)
                .when();
    }

    private RequestSpecification prepareGetDeleteWhen() {
        return given()
                .port(port)
                .auth()
                .basic(USERNAME, PASSWORD)
                .when();
    }

}
