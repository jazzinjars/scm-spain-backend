package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class UserNotFoundException extends FriendsException {

    private final String username;

    public UserNotFoundException(String username) {
        super("user-not-found-" + username);
        this.username = username;
    }

    public String getUsername() {
	return username;
    }
}
