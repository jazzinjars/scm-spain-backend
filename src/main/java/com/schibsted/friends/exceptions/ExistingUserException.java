package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class ExistingUserException extends FriendsException {

    private final String username;

    public ExistingUserException(String username) {
        super("existing-user-" + username);
        this.username = username;
    }

    public String getUsername() {
	return username;
    }
}
