package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class AloneUserException extends FriendsException {

    private final String username;

    public AloneUserException(String username) {
        super("forever-alone-user-" + username);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
