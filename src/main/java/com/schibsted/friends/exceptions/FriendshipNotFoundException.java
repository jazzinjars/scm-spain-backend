package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class FriendshipNotFoundException extends FriendsException {

    private final String username;
    private final String friendof;

    public FriendshipNotFoundException(String username, String friendof) {
        super("friendship-not-found-" + username + "-with-" + friendof);
        this.username = username;
        this.friendof = friendof;
    }

    public String getUsername() {
	return username;
    }
}
