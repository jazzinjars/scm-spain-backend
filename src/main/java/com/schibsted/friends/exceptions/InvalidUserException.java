package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidUserException extends FriendsException {

    private final String username;
    private final String password;

    public InvalidUserException(String username, String password) {
        super("invalid-user-" + username + "-or-password-" + password);
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
	return username;
    }
}
