package com.schibsted.friends.exceptions;

public abstract class FriendsException extends RuntimeException {

    public FriendsException(String exception) {
        super(exception);
    }
}
