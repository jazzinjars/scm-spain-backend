package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class DeclinedFriendshipException extends FriendsException {

    private final String username;

    public DeclinedFriendshipException(String username) {
        super("declined-friendship-" + username);
        this.username = username;
    }

    public String getUsername() {
	return username;
    }
}
