package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class IncorrectPasswordException extends FriendsException {

    private final String username;

    public IncorrectPasswordException(String username) {
        super("incorrect-password-of-" + username);
        this.username = username;
    }

    public String getUsername() {
	return username;
    }
}
