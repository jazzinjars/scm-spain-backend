package com.schibsted.friends.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_ACCEPTABLE)
public class HiddenUserException extends FriendsException {

    private final String username;

    public HiddenUserException(String username) {
        super("hidden-user-" + username);
        this.username = username;
    }

    public String getUsername() {
        return username;
    }
}
