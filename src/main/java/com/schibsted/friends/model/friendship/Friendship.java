package com.schibsted.friends.model.friendship;

import com.schibsted.friends.model.user.UserStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

@Entity
@Table(name = "friendships")
@Data
@AllArgsConstructor
@XmlRootElement
@ToString
public class Friendship {

    @TableGenerator(name = "friendship_gen", table = "id_gen", pkColumnName = "gen_name", valueColumnName = "gen_val", pkColumnValue = "friendship_gen", initialValue = 2000, allocationSize = 100)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "friendship_gen")
    private Long id;

    @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Username must contain only alphanumeric characters")
    @Column(name = "username", length = 10, nullable = false)
    @NotNull
    private String username;

    @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Username must contain only alphanumeric characters")
    @Column(name = "friendof", length = 10, nullable = false)
    @NotNull
    private String friendof;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @NotNull
    private FriendshipStatus status;

    @Column(name = "friends_date")
    private LocalDateTime friendsDate;

    public Friendship() {
        this.status = FriendshipStatus.PENDING;
    }
}
