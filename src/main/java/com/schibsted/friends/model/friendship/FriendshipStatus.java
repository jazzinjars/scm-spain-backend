package com.schibsted.friends.model.friendship;

import javax.xml.bind.annotation.XmlEnumValue;

public enum FriendshipStatus {
    @XmlEnumValue("PENDING") PENDING,
    @XmlEnumValue("ACCEPTED") ACCEPTED,
    @XmlEnumValue("DECLINED") DECLINED
}
