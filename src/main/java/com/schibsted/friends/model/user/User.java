package com.schibsted.friends.model.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
@Data
@AllArgsConstructor
@XmlRootElement
@ToString
public class User {

    @TableGenerator(name = "user_gen", table = "id_gen", pkColumnName = "gen_name", valueColumnName = "gen_val", pkColumnValue = "user_gen", initialValue = 1000, allocationSize = 100)
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "user_gen")
    private Long id;

    @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
    @Pattern(regexp = "^[a-zA-Z0-9]*$", message = "Username must contain only alphanumeric characters")
    @Column(name = "username", length = 10, nullable = false)
    @NotNull
    private String username;

    @Size(min = 1, max = 256)
    @Column(name = "password", length = 256, nullable = false)
    @NotNull
    private String password;

    @Column(name = "signup")
    private LocalDateTime signup;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    @NotNull
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    @Column(name = "user_role")
    @NotNull
    private UserRole role;

    private boolean hidden = false;
    private int declinations = 0;

    public User() {
        this.status = UserStatus.NOFRIENDS;
        this.role = UserRole.USER;
    }

    public void increaseDeclinations() {
        declinations++;
    }

}
