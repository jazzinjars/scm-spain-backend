package com.schibsted.friends.model.user;

import javax.xml.bind.annotation.XmlEnumValue;

public enum UserStatus {
    @XmlEnumValue("ALONE") ALONE,
    @XmlEnumValue("NOFRIENDS") NOFRIENDS,
    @XmlEnumValue("FRIENDS") FRIENDS
}
