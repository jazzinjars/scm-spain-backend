package com.schibsted.friends.repository;

import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FriendshipRepository extends JpaRepository<Friendship, Long> {

    List<Friendship> findByUsername(@Param("username") String username);
    Optional<Friendship> findByUsernameAndFriendof(@Param("username") String username, @Param("friendof") String friend);
    List<Friendship> findByUsernameAndStatus(@Param("username") String username, @Param("status") FriendshipStatus status);
    List<Friendship> findByFriendofAndStatus(@Param("friendof") String friendof, @Param("status") FriendshipStatus status);
}
