package com.schibsted.friends.controller;

import com.schibsted.friends.exceptions.FriendshipNotFoundException;
import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.service.friendship.FriendshipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.security.AccessControlException;
import java.util.List;

@RestController
@Validated
@RequestMapping(value = "/friends/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "Friendship", description = "API for Friendships management", tags = {"Friendships"})
@CrossOrigin(origins = "*", allowedHeaders="*", exposedHeaders = "Authorization", allowCredentials = "true")
public class FriendshipController {

    private static final String VALIDATION_REGEX = "^[a-zA-Z0-9]*$";

    @Autowired
    private FriendshipService friendshipService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @ApiOperation(value = "Get all Friendships from Friends App", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @GetMapping("friendships")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<Friendship>> getAllFriendships() {
        return ResponseEntity.ok(friendshipService.getAllFriendships());
    }

    @ApiOperation(value = "Request Friendship from User to User", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @PostMapping("friendships/request/{friend}")
    public ResponseEntity<Friendship> requestFriendship(@RequestBody @ApiParam(value = "User", required = true) @Valid User user,
                                                        @RequestParam("friend")
                                                        @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                        String friend) {

        checkLoggedUserHisselfRetrievingInfo(user.getUsername());
        return this.friendshipService.requestFriendship(user, friend).map(ResponseEntity::ok).orElseThrow(() -> new FriendshipNotFoundException(user.getUsername(), friend));
    }

    @ApiOperation(value = "Accept Friendship from User", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @PutMapping("friendships/accept/{friend}")
    public ResponseEntity<Friendship> acceptFriendship(@RequestBody @ApiParam(value = "User", required = true) @Valid User user,
                                                        @RequestParam("friend")
                                                        @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                                String friend) {
        checkLoggedUserHisselfRetrievingInfo(user.getUsername());
        return this.friendshipService.acceptFriendship(user, friend).map(ResponseEntity::ok).orElseThrow(() -> new FriendshipNotFoundException(user.getUsername(), friend));
    }

    @ApiOperation(value = "Decline Friendship from User", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @PutMapping("friendships/decline/{friend}")
    public ResponseEntity<Friendship> declineFriendship(@RequestBody @ApiParam(value = "User", required = true) @Valid User user,
                                                        @RequestParam("friend")
                                                        @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                                String friend) {
        checkLoggedUserHisselfRetrievingInfo(user.getUsername());
        return this.friendshipService.declineFriendship(user, friend).map(ResponseEntity::ok).orElseThrow(() -> new FriendshipNotFoundException(user.getUsername(), friend));
    }

    @ApiOperation(value = "Get Pending Friendships of User", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @GetMapping("friendships/pendings/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<Friendship>> getPendingsFriendships(@RequestParam("username")
                                                                    @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                                                    @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                                            String username) {
        checkLoggedUserHisselfRetrievingInfo(username);
        return ResponseEntity.ok(friendshipService.getFriendshipsByFriendofAndStatus(username, FriendshipStatus.PENDING));
    }

    private void checkLoggedUserHisselfRetrievingInfo(String username) {
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        if (!username.equals(currentUserName)) throw new AccessControlException("You don't have permissions to do the action");
    }

}
