package com.schibsted.friends.controller;

import com.schibsted.friends.exceptions.ExistingUserException;
import com.schibsted.friends.exceptions.UserNotFoundException;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.service.user.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.PermitAll;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.security.AccessControlException;
import java.util.List;
import java.util.Optional;

@RestController
@Validated
@RequestMapping(value = "/friends/v1", produces = MediaType.APPLICATION_JSON_VALUE)
@Api(value = "User", description = "API for User management", tags = {"User"})
@CrossOrigin(origins = "*", allowedHeaders="*", exposedHeaders = "Authorization", allowCredentials = "true")
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class);
    private static final String VALIDATION_REGEX = "^[a-zA-Z0-9]*$";

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @ApiOperation(value = "User SignUp", notes = "")
    @PostMapping(value = "users/signup", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @PermitAll
    public ResponseEntity<User> signupUser(@RequestParam("username")
                                           @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                           @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                   String username,
                                           @RequestParam("password")
                                           @Size(min = 5, max = 10, message = "Password should have between 5 to 10 characters")
                                           @Pattern(regexp = VALIDATION_REGEX, message = "Password must contain only alphanumeric characters")
                                                   String password) {

        Optional<User> signupUser = this.userService.createUser(username, passwordEncoder.encode(password));
        authenticateUserOnSecurityContext(username, password);
        return signupUser.map(ResponseEntity::ok).orElseThrow(() -> new ExistingUserException(username));
    }

    @ApiOperation(value = "User Login", notes = "")
    @PostMapping(value = "users/login", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    @PermitAll
    public ResponseEntity<User> loginUser(@RequestParam("username")
                                           @Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                           @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                   String username,
                                           @RequestParam("password")
                                           @Size(min = 5, max = 10, message = "Password should have between 5 to 10 characters")
                                           @Pattern(regexp = VALIDATION_REGEX, message = "Password must contain only alphanumeric characters")
                                                   String password) {

        Optional<User> loggedUser = this.userService.validateUser(username, password);
        authenticateUserOnSecurityContext(username, password);

        return loggedUser.map(ResponseEntity::ok).orElseThrow(() -> new UserNotFoundException(username));
    }

    @ApiOperation(value = "Get all Users from Friends App", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @GetMapping("users")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<User>> getAllUsers() {
        return ResponseEntity.ok(userService.findAllUsers());
    }

    @ApiOperation(value = "Get User by username", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @GetMapping("users/{username}")
    public ResponseEntity<User> getUser(@Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                        @PathVariable("username") String username) {
        return this.userService.findUser(username).map(ResponseEntity::ok).orElseThrow(() -> new UserNotFoundException(username));
    }

    @ApiOperation(value = "Get User Friends", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @GetMapping("users/friends/{username}")
    public ResponseEntity<List<User>> getUserFriends(@Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                                    @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                                    @PathVariable("username") String username) {
        checkLoggedUserHisselfRetrievingInfo(username);
        return ResponseEntity.ok(userService.getUserFriends(username));
    }

    @ApiOperation(value = "Update a User by username", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @PutMapping("users/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> updateUser(@Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                        @PathVariable("username") String username) {
        return this.userService.findUser(username)
                .map(user -> {
                    userService.deleteUser(user);
                    return ResponseEntity.noContent().build();
                }).orElseThrow(() -> new UserNotFoundException(username));
    }

    @ApiOperation(value = "Delete a User by username", notes = "", authorizations = {@Authorization(value = "basicAuth")})
    @DeleteMapping("users/{username}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<?> deleteUser(@Size(min = 5, max = 10, message = "Username should have between 5 to 10 characters")
                                        @Pattern(regexp = VALIDATION_REGEX, message = "Username must contain only alphanumeric characters")
                                        @PathVariable("username") String username) {
        return this.userService.findUser(username)
                .map(user -> {
                    userService.deleteUser(user);
                    return ResponseEntity.noContent().build();
                }).orElseThrow(() -> new UserNotFoundException(username));
    }

    private void checkLoggedUserHisselfRetrievingInfo(String username) {
        String currentUserName = "";
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            currentUserName = authentication.getName();
        }
        if (!username.equals(currentUserName)) throw new AccessControlException("You don't have permissions to view other User Profiles");
    }

    private void authenticateUserOnSecurityContext(String username, String password) {
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(username, password));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        logger.info("UserLogged:[" + username + "]");
    }
}
