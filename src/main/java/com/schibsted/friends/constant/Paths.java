package com.schibsted.friends.constant;

public class Paths {

    public static final String VERSION = "/friends/v1/";
    public static final String USERS = "users/";
    public static final String SIGNUP = "signup/";
    public static final String FRIENDS = "friends/";
    public static final String FRIENDSHIPS = "friendships/";
    public static final String REQUEST = "request/";
    public static final String ACCEPT = "accept/";
    public static final String DECLINE = "decline/";
    public static final String PENDINGS = "pendings/";
}
