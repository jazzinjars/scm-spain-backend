package com.schibsted.friends.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket productApi() {
	return new Docket(DocumentationType.SWAGGER_2)
		.select()
		.apis(RequestHandlerSelectors.basePackage("com.schibsted.friends.controller"))
		.paths(regex("/friends/v1.*"))
		.build()
		.apiInfo(metaData())
		.securitySchemes(securityScheme());
    }

    private ApiInfo metaData() {
	ApiInfo apiInfo = new ApiInfo(
		"Friends Application Programming Interface (API)",
		"Restful API to manage the Friends Application to be evaluated.",
		"V1.0",
		"Terms of service",
		new Contact("Héctor Ardila López", "https://github.com/jazzinjars/", "hectorardila@gmail.com"),
		"Apache License Version 2.0",
		"https://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());
	return apiInfo;
    }

    private List<SecurityScheme> securityScheme() {
	List<SecurityScheme> schemeList = new ArrayList<>();
	schemeList.add(new BasicAuth("basicAuth"));
	return schemeList;
    }
}
