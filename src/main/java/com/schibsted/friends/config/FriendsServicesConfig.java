package com.schibsted.friends.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = "com.schibsted.friends.service")
public class FriendsServicesConfig {
}
