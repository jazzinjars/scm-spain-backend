package com.schibsted.friends.service.user;

import com.schibsted.friends.model.user.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component("beforeCreateFriendsUserValidator")
public class UserValidator implements Validator {

    private static final String PASSWORD_ATTRIBUTE = "password";
    private static final String USERNAME_ATTRIBUTE = "username";
    private static final int MIN_LENGTH_PASSWORD = 8;
    private static final int MAX_LENGTH_PASSWORD = 12;
    private static final int MIN_LENGTH_USERNAME = 5;
    private static final int MAX_LENGTH_USERNAME = 10;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        String username = user.getUsername();
        String password = user.getPassword();

        validateAlphanumericType(password, errors, PASSWORD_ATTRIBUTE);
        validateAlphanumericType(username, errors, USERNAME_ATTRIBUTE);
        validateAttributeLength(password, MIN_LENGTH_PASSWORD, MAX_LENGTH_PASSWORD, errors, PASSWORD_ATTRIBUTE);
        validateAttributeLength(password, MIN_LENGTH_USERNAME, MAX_LENGTH_USERNAME, errors, USERNAME_ATTRIBUTE);
    }

    private void validateAlphanumericType(String value, Errors errors, String attribute) {
        Pattern pattern = Pattern.compile("^[a-zA-Z0-9]*$");
        Matcher matcher = pattern.matcher(value);
        if(!matcher.matches()) {
            errors.rejectValue(attribute, attribute + ".noAlphanumeric");
        }
    }

    private void validateAttributeLength(String value, int min, int max, Errors errors, String attribute) {
        if (value.length() > max || value.length() < min) {
            errors.rejectValue(attribute, attribute + ".invalidLength");
        }
    }

}
