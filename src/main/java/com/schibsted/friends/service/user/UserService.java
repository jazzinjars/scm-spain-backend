package com.schibsted.friends.service.user;

import com.schibsted.friends.model.user.User;

import java.util.List;
import java.util.Optional;

public interface UserService {

    List<User> findAllUsers();
    Optional<User> findUser(String username);
    List<User> getUserFriends(String username);
    Optional<User> createUser(String username, String password);
    Optional<User> updateUser(User user);
    void deleteUser(User user);
    User saveUser(User user);
    Optional<User> rejectUserFriendship(String username);
    void updateUsersFriends(String username, String friend);
    Optional<User> validateUser(String username, String password);
}
