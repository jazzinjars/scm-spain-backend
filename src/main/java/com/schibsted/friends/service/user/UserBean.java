package com.schibsted.friends.service.user;

import com.schibsted.friends.exceptions.IncorrectPasswordException;
import com.schibsted.friends.exceptions.InvalidUserException;
import com.schibsted.friends.exceptions.UserNotFoundException;
import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.model.user.UserStatus;
import com.schibsted.friends.repository.UserRepository;
import com.schibsted.friends.service.friendship.FriendshipBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.PersistenceException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserBean implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FriendshipBean friendshipBean;

    @Override
    public List<User> findAllUsers() {
	return userRepository.findAll();
    }

    @Override
    public Optional<User> findUser(String username) {
	return userRepository.findByUsername(username);
    }

    @Override
    public Optional<User> createUser(String username, String password) {
        try {
            Optional<User> existingUser = userRepository.findByUsername(username);
            User userToCreate = null;
            if (!existingUser.isPresent()) {
                userToCreate = new User();
                userToCreate.setUsername(username);
                userToCreate.setPassword(password);
                userToCreate.setSignup(LocalDateTime.now());
                this.userRepository.save(userToCreate);
                return Optional.of(userToCreate);
            }
        } catch (PersistenceException exception) {
            throw new InvalidUserException(username, password);
        }
        return Optional.empty();
    }

    @Override
    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    @Override
    public Optional<User> updateUser(User user) {
        return Optional.of(userRepository.save(user));
    }

    @Override
    public List<User> getUserFriends(String username) {

        List<Friendship> friendships = friendshipBean.getUserFriends(username);
        List<String> friends = friendships.
                                    stream().map(friend -> friend.getFriendof()).collect(Collectors.toList());
        return friends.
                stream().map(f -> userRepository.findByUsername(f).get()).collect(Collectors.toList());
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> rejectUserFriendship(String username) {
        User rejectedUser = findUser(username).get();
        rejectedUser.increaseDeclinations();
        boolean foreverAlone = checkAndSetDeclinations(rejectedUser);

        if (foreverAlone) {
            rejectedUser.setStatus(UserStatus.ALONE);
        }
        rejectedUser = userRepository.save(rejectedUser);
        return Optional.of(rejectedUser);
    }

    @Override
    public void updateUsersFriends(String username, String friend) {
        User fromUser = findUser(username).get();
        User toUser = findUser(friend).get();
        fromUser.setStatus(UserStatus.FRIENDS);
        toUser.setStatus(UserStatus.FRIENDS);

        saveUser(fromUser);
        saveUser(toUser);
    }

    @Override
    public Optional<User> validateUser(String username, String password) {
        Optional<User> existingUser = userRepository.findByUsername(username);
        if (!existingUser.isPresent()) {
            throw new UserNotFoundException(username);
        } else {
            if (passwordMatches(password, existingUser.get())) {
                return existingUser;
            } else {
                throw new IncorrectPasswordException(username);
            }
        }
    }

    private boolean passwordMatches(String password, User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder.matches(password, user.getPassword());
    }

    private boolean checkAndSetDeclinations(User user) {
        if (user.getDeclinations() == 3) {
            user.setStatus(UserStatus.ALONE);
            return true;
        } else {
            return false;
        }
    }
}
