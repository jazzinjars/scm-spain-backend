package com.schibsted.friends.service;

import com.schibsted.friends.model.user.User;
import com.schibsted.friends.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service(value = "userDetailsService")
public class FriendsUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
	User user = userRepository.findByUsername(username).get();

	if (user == null) {
	    throw new UsernameNotFoundException(String.format("The username %s doesn't exist", username));
	}

	List<GrantedAuthority> authorities = new ArrayList<>();
	authorities.add(new SimpleGrantedAuthority(user.getRole().toString()));

	UserDetails userDetails = new org.springframework.security.core.userdetails.User(user.getUsername(),
		user.getPassword(), authorities);
	return userDetails;
    }
}
