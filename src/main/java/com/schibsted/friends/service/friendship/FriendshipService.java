package com.schibsted.friends.service.friendship;

import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import com.schibsted.friends.model.user.User;

import java.util.List;
import java.util.Optional;

public interface FriendshipService {

    List<Friendship> getUserFriends(String username);
    List<Friendship> getAllFriendships();
    Optional<Friendship> requestFriendship(User user, String friend);
    Optional<Friendship> acceptFriendship(User user, String friend);
    Optional<Friendship> declineFriendship(User user, String friend);
    List<Friendship> getFriendshipsByUsernameAndStatus(String username, FriendshipStatus status);
    List<Friendship> getFriendshipsByFriendofAndStatus(String username, FriendshipStatus status);
}
