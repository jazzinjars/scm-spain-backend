package com.schibsted.friends.service.friendship;

import com.schibsted.friends.exceptions.*;
import com.schibsted.friends.model.friendship.Friendship;
import com.schibsted.friends.model.friendship.FriendshipStatus;
import com.schibsted.friends.model.user.User;
import com.schibsted.friends.model.user.UserStatus;
import com.schibsted.friends.repository.FriendshipRepository;
import com.schibsted.friends.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class FriendshipBean implements FriendshipService {

    @Autowired
    private FriendshipRepository friendshipRepository;

    @Autowired
    private UserService userService;

    @Override
    public List<Friendship> getUserFriends(String username) {
        return friendshipRepository.findByUsernameAndStatus(username, FriendshipStatus.ACCEPTED);
    }

    @Override
    public List<Friendship> getAllFriendships() {
        return friendshipRepository.findAll();
    }

    @Override
    public Optional<Friendship> requestFriendship(User user, String friend) {

        Optional<Friendship> friendship = friendshipRepository.findByUsernameAndFriendof(user.getUsername(), friend);
        if (friendship.isPresent()) {
            return processExistingFriendship(friendship, FriendshipStatus.PENDING);
        } else {
            return processRequestedFriendship(user, friend);
        }
    }

    @Override
    public Optional<Friendship> acceptFriendship(User user, String friend) {

        Optional<Friendship> friendship = friendshipRepository.findByUsernameAndFriendof(friend, user.getUsername());
        if (friendship.isPresent()) {
            friendship = processExistingFriendship(friendship, FriendshipStatus.ACCEPTED);
            return processFriendshipToBeAccepted(friendship);
        } else {
            throw new FriendshipNotFoundException(user.getUsername(), friend);
        }
    }

    @Override
    public Optional<Friendship> declineFriendship(User user, String friend) {

        Optional<Friendship> friendship = friendshipRepository.findByUsernameAndFriendof(friend, user.getUsername());
        if (friendship.isPresent()) {
            friendship = processExistingFriendship(friendship, FriendshipStatus.DECLINED);
            return processFriendshipToBeDeclined(friendship);
        } else {
            throw new FriendshipNotFoundException(user.getUsername(), friend);
        }

    }

    @Override
    public List<Friendship> getFriendshipsByUsernameAndStatus(String username, FriendshipStatus status) {
        return friendshipRepository.findByUsernameAndStatus(username, status);
    }

    @Override
    public List<Friendship> getFriendshipsByFriendofAndStatus(String username, FriendshipStatus status) {
        return friendshipRepository.findByFriendofAndStatus(username, status);
    }

    private Optional<Friendship> processExistingFriendship(Optional<Friendship> friendship, FriendshipStatus toStatus) {

        FriendshipStatus currentStatus = friendship.get().getStatus();
        if ((currentStatus == toStatus) && (currentStatus != FriendshipStatus.DECLINED)) {
            return friendship;
        } else {
            switch (currentStatus) {
                case DECLINED: throw new DeclinedFriendshipException(friendship.get().getUsername());
                case ACCEPTED:
                case PENDING:
                default: return friendship;
            }
        }
    }

    private Optional<Friendship> processFriendshipToBeAccepted(Optional<Friendship> friendship) {

        friendship.get().setStatus(FriendshipStatus.ACCEPTED);
        friendship = Optional.of(friendshipRepository.save(friendship.get()));

        createFriendshipWithStatus(friendship.get(), FriendshipStatus.ACCEPTED);
        userService.updateUsersFriends(friendship.get().getUsername(), friendship.get().getFriendof());

        return friendship;
    }

    private Optional<Friendship> processFriendshipToBeDeclined(Optional<Friendship> friendship) {

        friendship.get().setStatus(FriendshipStatus.DECLINED);
        friendship = Optional.of(friendshipRepository.save(friendship.get()));

        createFriendshipWithStatus(friendship.get(), FriendshipStatus.DECLINED);
        userService.rejectUserFriendship(friendship.get().getUsername());

        return friendship;
    }

    private Optional<Friendship> processRequestedFriendship(User user, String friend) {

        Optional<User> userToBeFriend = userService.findUser(friend);
        if (!userToBeFriend.isPresent()) {
            throw new UserNotFoundException(friend);
        }
        checkAloneUser(userToBeFriend.get());
        checkHiddenUser(userToBeFriend.get());

        Friendship newFriendship = new Friendship();
        newFriendship.setUsername(user.getUsername());
        newFriendship.setFriendof(friend);
        newFriendship.setFriendsDate(LocalDateTime.now());
        newFriendship = friendshipRepository.save(newFriendship);

        return Optional.of(newFriendship);
    }

    private void checkAloneUser(User user) throws AloneUserException {
        if (user.getStatus() == UserStatus.ALONE) {
            throw new AloneUserException(user.getUsername());
        }
    }

    private void checkHiddenUser(User user) throws HiddenUserException {
        if (user.isHidden()) {
            throw new AloneUserException(user.getUsername());
        }
    }

    private void createFriendshipWithStatus(Friendship friendship, FriendshipStatus status) {
        Friendship acceptedFriendship = new Friendship();
        acceptedFriendship.setStatus(status);
        String user = friendship.getFriendof();
        String friend = friendship.getUsername();

        if (status == FriendshipStatus.DECLINED) {
            user = friendship.getUsername();
            friend = friendship.getFriendof();
        }
        acceptedFriendship.setUsername(user);
        acceptedFriendship.setFriendof(friend);
        acceptedFriendship.setFriendsDate(LocalDateTime.now());

        friendshipRepository.save(acceptedFriendship);
    }
}
