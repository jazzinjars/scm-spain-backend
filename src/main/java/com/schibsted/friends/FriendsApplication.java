package com.schibsted.friends;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FriendsApplication {

    private static final Logger logger = LogManager.getLogger(FriendsApplication.class);

    public static void main(String[] args) {
	logger.info("==== RUNNING FRIENDS APP ====");
	SpringApplication.run(FriendsApplication.class, args);
    }
}
