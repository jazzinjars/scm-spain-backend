
INSERT INTO users (id, username, password, signup, hidden, declinations, status, user_role) VALUES (1, 'arya1', '$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.', '2015-12-01 08:00:00.0', FALSE, 0, 'NOFRIENDS', 'ADMIN');
INSERT INTO users (id, username, password, signup, hidden, declinations, status, user_role) VALUES (2, 'sansa', '$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.', '2015-12-01 08:00:00.0', FALSE, 0, 'NOFRIENDS', 'USER');
INSERT INTO users (id, username, password, signup, hidden, declinations, status, user_role) VALUES (3, 'cersei', '$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.', '2015-12-01 08:00:00.0', TRUE, 3, 'ALONE', 'USER');
INSERT INTO users (id, username, password, signup, hidden, declinations, status, user_role) VALUES (4, 'catelyn', '$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.', '2015-12-01 08:00:00.0', FALSE, 0, 'NOFRIENDS', 'USER');
INSERT INTO users (id, username, password, signup, hidden, declinations, status, user_role) VALUES (5, 'daenerys', '$2a$10$ukWgiVIkyNkjKkjbqoWn6e2h28kVmxOw/X6Kbpp9ozVi6LQZq/Ks.', '2015-12-01 08:00:00.0', FALSE, 2, 'NOFRIENDS', 'USER');

INSERT INTO friendships (id, username, friendof, status, friends_date) VALUES (1, 'arya1', 'sansa', 'PENDING', '2015-12-01 08:00:00.0');
INSERT INTO friendships (id, username, friendof, status, friends_date) VALUES (2, 'arya1', 'catelyn', 'PENDING', '2015-12-01 08:00:00.0');
INSERT INTO friendships (id, username, friendof, status, friends_date) VALUES (3, 'daenerys', 'arya1', 'PENDING', '2015-12-01 08:00:00.0');